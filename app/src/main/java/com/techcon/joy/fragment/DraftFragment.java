package com.techcon.joy.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.techcon.joy.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class DraftFragment extends Fragment {


    public DraftFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_draft, container, false);
    }

}
