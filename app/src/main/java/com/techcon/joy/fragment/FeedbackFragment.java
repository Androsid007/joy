package com.techcon.joy.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.techcon.joy.NetworkService.CommonUtility;
import com.techcon.joy.NetworkService.RetrofitTask;
import com.techcon.joy.NetworkService.RetrofitTaskListener;
import com.techcon.joy.NetworkService.ServerConfigStage;
import com.techcon.joy.R;
import com.techcon.joy.activity.HomeActivity;
import com.techcon.joy.model.Feedback;
import com.techcon.joy.model.FeedbackResponse;

import retrofit.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FeedbackFragment extends Fragment implements RetrofitTaskListener<FeedbackResponse> {

    private ProgressDialog progressDialog;
    private String respo;
    private Button send;
    public static final String mypreference = "mypref";
    private String userId,userFeedback,caseId;
    private SharedPreferences sharedpreferences;
    private EditText feedbackTxt;


    public FeedbackFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_feedback, container, false);

        sharedpreferences = getActivity().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        userId = sharedpreferences.getString("userId","");
        caseId = sharedpreferences.getString("caseId","");

        send = (Button)view.findViewById(R.id.send_btn);
        feedbackTxt = (EditText)view.findViewById(R.id.edit_fed);


        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userFeedback = feedbackTxt.getText().toString().trim();
                String url = String.format(ServerConfigStage.SEND_FEEDBACK());
                callLoginService(url);
            }
        });


        return view;
    }

    public void showProgreass() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.cancel();
    }

    public void callLoginService(String url) {
        showProgreass();

       /* ArrayList<Feedback> feedbackArrayList = new ArrayList<Feedback>();
*/
        Feedback feedback =new Feedback();
        feedback.setCaseID(caseId);
        feedback.setIsConvenced("1");
        feedback.setResponse(userFeedback);
        feedback.setUserID(userId);



        try {


            url = url.replace(" ", "%20");
            RetrofitTask task = new RetrofitTask<FeedbackResponse>(FeedbackFragment.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.FEEDBACK, url, getActivity(), feedback);
            task.execute();



        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }




    }

    @Override
    public void onRetrofitTaskComplete(Response<FeedbackResponse> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response.isSuccess()) {
            if (response.body() != null) {
                if (response.body().getStatusMessage().equals("Success")){
                //if (response.body().getStatusMessage().equals("Success"))
                Toast.makeText(getActivity(),"Feedback Successfull",Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getActivity(), HomeActivity.class);
                    startActivity(intent);

                }else {
                    Toast.makeText(getActivity(),"Feedback Not Submitted",Toast.LENGTH_LONG).show();
                }

            }
        }
    }
    @Override
    public void onRetrofitTaskFailure(Throwable t) {

    }
}
