package com.techcon.joy.fragment;


import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.OnInitializedListener;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.techcon.joy.NetworkService.CommonUtility;
import com.techcon.joy.NetworkService.RetrofitTask;
import com.techcon.joy.NetworkService.RetrofitTaskListener;
import com.techcon.joy.NetworkService.ServerConfigStage;
import com.techcon.joy.R;
import com.techcon.joy.activity.HomeActivity;
import com.techcon.joy.activity.LoginActivity;
import com.techcon.joy.adapter.RecyclerViewDataAdapter;
import com.techcon.joy.model.Config;
import com.techcon.joy.model.Like;
import com.techcon.joy.model.LikeParams;
import com.techcon.joy.model.Login;
import com.techcon.joy.model.SectionDataModel;
import com.techcon.joy.model.UserInfoResponse;
import com.techcon.joy.model.VideoThumbnail;

import java.util.ArrayList;

import retrofit.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class PlayerFragment extends Fragment implements RetrofitTaskListener<Like> {
    private FragmentActivity myContext;

    private YouTubePlayer YPlayer;
    public static final String mypreference = "mypref";
    private String userType;
    private SharedPreferences sharedpreferences;
    ArrayList<SectionDataModel> allSampleData;
    private ImageView rate,like;
    private  boolean isStopped = false;
    private ProgressDialog progressDialog;
    private Dialog rankDialog;
    private RatingBar ratingBar;
    private String rateValue;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_player, container, false);

        sharedpreferences = getActivity().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        userType = sharedpreferences.getString("userType","");

        rate = (ImageView)rootView.findViewById(R.id.rate_img);
        like = (ImageView)rootView.findViewById(R.id.like_img);

        if (userType.equals("Type 1")){
            rate.setVisibility(View.GONE);
            like.setVisibility(View.GONE);
        }else if (userType.equals("Type 2")){
            rate.setVisibility(View.VISIBLE);
            like.setVisibility(View.VISIBLE);
        }

        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = String.format(ServerConfigStage.GET_LIKE());
                callLikeService(url);
            }
        });

        rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rankDialog = new Dialog(getActivity(), R.style.FullHeightDialog);
                rankDialog.setContentView(R.layout.rank_dialog);
                rankDialog.setCancelable(true);
                ratingBar = (RatingBar)rankDialog.findViewById(R.id.dialog_ratingbar);
                ratingBar.setRating((float) 2.0);

                Button updateButton = (Button) rankDialog.findViewById(R.id.rank_dialog_button);
                updateButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        rankDialog.dismiss();
                        rateValue = String.valueOf(ratingBar.getRating());
                        String url = String.format(ServerConfigStage.GET_LIKE());
                        callLikeService(url);
                    }
                });
                //now that the dialog is set up, it's time to show it
                rankDialog.show();
            }
        });



        YouTubePlayerSupportFragment youTubePlayerFragment = YouTubePlayerSupportFragment.newInstance();
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.add(R.id.youtube_fragment, youTubePlayerFragment).commit();

        youTubePlayerFragment.initialize(Config.DEVELOPER_KEY, new OnInitializedListener() {

            @Override
            public void onInitializationSuccess(Provider arg0, YouTubePlayer youTubePlayer, boolean b) {
                if (!b) {

                    if (userType.equals("Type 1")){
                    YPlayer = youTubePlayer;
                    YPlayer.setFullscreen(false);
                    YPlayer.loadVideo("PgKzrFXPP80");
                    YPlayer.setPlaybackEventListener(new YouTubePlayer.PlaybackEventListener() {
                        @Override
                        public void onPlaying() {

                        }

                        @Override
                        public void onPaused() {

                        }

                        @Override
                        public void onStopped() {
                            Toast.makeText(getActivity(),"Ends",Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onBuffering(boolean b) {

                        }

                        @Override
                        public void onSeekTo(int i) {

                        }
                    });
                    YPlayer.setPlayerStateChangeListener(new YouTubePlayer.PlayerStateChangeListener() {
                        @Override
                        public void onLoading() {

                        }

                        @Override
                        public void onLoaded(String s) {

                        }

                        @Override
                        public void onAdStarted() {

                        }

                        @Override
                        public void onVideoStarted() {

                            Fragment f = getFragmentManager().findFragmentByTag("feedback");
                            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                            if(f!=null) transaction.remove(f);
                            transaction.commit();

                        }

                        @Override
                        public void onVideoEnded() {
                            isStopped=true;
                            Toast.makeText(getActivity(),"Ends",Toast.LENGTH_LONG).show();
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setMessage("Are you Convinced")
                                    .setCancelable(false)
                                    .setNegativeButton("No",new DialogInterface.OnClickListener(){
                                        public void onClick(DialogInterface dialog, int id) {
                                            //do things
                                            Fragment childFragment = new FeedbackFragment();
                                            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                                            transaction.add(R.id.bottom_container, childFragment,"feedback").commit();
                                        }
                                    })
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            //do things
                                        }
                                    });

                            AlertDialog alert = builder.create();
                            alert.show();
                        }

                        @Override
                        public void onError(YouTubePlayer.ErrorReason errorReason) {

                        }
                    });
                    }else if(userType.equals("Type 2")){
                        YPlayer = youTubePlayer;
                        YPlayer.setFullscreen(false);
                        YPlayer.loadVideo("PgKzrFXPP80");
                        YPlayer.setPlaybackEventListener(new YouTubePlayer.PlaybackEventListener() {
                            @Override
                            public void onPlaying() {

                            }

                            @Override
                            public void onPaused() {

                            }

                            @Override
                            public void onStopped() {
                                Toast.makeText(getActivity(),"Ends",Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onBuffering(boolean b) {

                            }

                            @Override
                            public void onSeekTo(int i) {

                            }
                        });
                        YPlayer.setPlayerStateChangeListener(new YouTubePlayer.PlayerStateChangeListener() {
                            @Override
                            public void onLoading() {

                            }

                            @Override
                            public void onLoaded(String s) {

                            }

                            @Override
                            public void onAdStarted() {

                            }

                            @Override
                            public void onVideoStarted() {


                            }

                            @Override
                            public void onVideoEnded() {
                                Toast.makeText(getActivity(),"Ends",Toast.LENGTH_LONG).show();

                            }

                            @Override
                            public void onError(YouTubePlayer.ErrorReason errorReason) {

                            }
                        });
                    }
                    YPlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
                    YPlayer.play();
                }
            }

            @Override
            public void onInitializationFailure(Provider arg0, YouTubeInitializationResult arg1) {
                // TODO Auto-generated method stub

            }
        });

       /* videoRecyclerView = (RecyclerView) rootView.findViewById(R.id.similar_video_recycler);

        allSampleData = new ArrayList<SectionDataModel>();
        createDummyData();


        videoRecyclerView.setHasFixedSize(true);

        RecyclerViewDataAdapter adapter = new RecyclerViewDataAdapter(getActivity(), allSampleData);

        videoRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        videoRecyclerView.setAdapter(adapter);*/

        return rootView;
    }

    public void createDummyData() {

        SectionDataModel dm = new SectionDataModel();

        dm.setHeaderTitle("Similar Videos");



        ArrayList<VideoThumbnail> singleItem = new ArrayList<VideoThumbnail>();
        for (int j = 0; j <= 5; j++) {
            singleItem.add(new VideoThumbnail("Item " + j, "URL " + j));
        }

        //dm.setAllItemsInSection(singleItem);

        allSampleData.add(dm);


    }

    public void showProgreass() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.cancel();
    }

    public void callLikeService(String url) {
        showProgreass();

        LikeParams likeParams = new LikeParams();
        likeParams.setUserid("2");
        likeParams.setVideoAction("video_like");
        likeParams.setActionVal("1");
        likeParams.setVideoid("3");

        try {
            url = url.replace(" ", "%20");
            RetrofitTask task = new RetrofitTask<Like>(PlayerFragment.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.LIKE, url, getActivity(), likeParams);
            task.execute();



        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void callRateService(String url) {
        showProgreass();

        LikeParams likeParams = new LikeParams();
        likeParams.setUserid("2");
        likeParams.setVideoAction("star_rating");
        likeParams.setActionVal("3");
        likeParams.setVideoid("3");

        try {
            url = url.replace(" ", "%20");
            RetrofitTask task = new RetrofitTask<Like>(PlayerFragment.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.LIKE, url, getActivity(), likeParams);
            task.execute();



        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRetrofitTaskComplete(Response<Like> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response.isSuccess()) {
            if (response.body() != null) {

                if (response.body().getStatusMessage().equals("Success")){
                    //if (response.body().getStatusMessage().equals("Success"))
                    Toast.makeText(getActivity(),response.body().getStatusMessage(),Toast.LENGTH_LONG).show();

                }else {
                    Toast.makeText(getActivity(),response.body().getStatusMessage(),Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();

    }
}
