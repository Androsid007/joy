package com.techcon.joy.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.techcon.joy.NetworkService.CommonUtility;
import com.techcon.joy.NetworkService.RetrofitTask;
import com.techcon.joy.NetworkService.RetrofitTaskListener;
import com.techcon.joy.NetworkService.ServerConfigStage;
import com.techcon.joy.R;
import com.techcon.joy.adapter.RecyclerViewDataAdapter;
import com.techcon.joy.model.AllVideo;
import com.techcon.joy.model.CategoryVideo;
import com.techcon.joy.model.LastestVideo;
import com.techcon.joy.model.SectionDataModel;
import com.techcon.joy.model.VideoList;
import com.techcon.joy.model.VideoThumbnail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener,RetrofitTaskListener<VideoList> {

    private SliderLayout mDemoSlider;
    ArrayList<SectionDataModel> allSampleData;
    private List<AllVideo> allVideoArrayList = new ArrayList<>();
    private List<CategoryVideo> categoryVideoArrayList =new ArrayList<>();
    private List<LastestVideo> lastestVideoArrayList=new ArrayList<>();
    private ProgressDialog progressDialog;
    public static final String mypreference = "mypref";
    private String userType;
    private SharedPreferences sharedpreferences;
    private RecyclerView my_recycler_view;


    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_home, container, false);

        sharedpreferences = getActivity().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        userType = sharedpreferences.getString("userType","");

        my_recycler_view = (RecyclerView) view.findViewById(R.id.my_recycler_view);


        allSampleData = new ArrayList<SectionDataModel>();

        getVideoList();






        mDemoSlider = (SliderLayout)view.findViewById(R.id.slider);

        HashMap<String,String> url_maps = new HashMap<String, String>();
        url_maps.put("Hannibal", "http://static2.hypable.com/wp-content/uploads/2013/12/hannibal-season-2-release-date.jpg");
        url_maps.put("Big Bang Theory", "http://tvfiles.alphacoders.com/100/hdclearart-10.png");
        url_maps.put("House of Cards", "http://cdn3.nflximg.net/images/3093/2043093.jpg");
        url_maps.put("Game of Thrones", "http://images.boomsbeat.com/data/images/full/19640/game-of-thrones-season-4-jpg.jpg");

        HashMap<String,Integer> file_maps = new HashMap<String, Integer>();
        file_maps.put("Hannibal",R.mipmap.banner);
        file_maps.put("Big Bang Theory",R.mipmap.banner);
        file_maps.put("House of Cards",R.mipmap.banner);
        file_maps.put("Game of Thrones", R.mipmap.banner);

        for(String name : file_maps.keySet()){
            TextSliderView textSliderView = new TextSliderView(getActivity());
            // initialize a SliderLayout
            textSliderView
                    .image(file_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra",name);

            mDemoSlider.addSlider(textSliderView);
        }
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mDemoSlider.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Invisible);
        mDemoSlider.setDuration(4000);
        mDemoSlider.addOnPageChangeListener(this);

        return view;
    }

    public void createDataForTypeOne() {
        for (int i = 1; i <= 3; i++) {
            SectionDataModel dm = new SectionDataModel();
            if (i==1){
                dm.setHeaderTitle("Categorized Videos");
                ArrayList<CategoryVideo> singleItem = new ArrayList<CategoryVideo>();
                for (int j = 0; j < categoryVideoArrayList.size(); j++) {
                    singleItem.add(new CategoryVideo("Item " + j));
                }
                dm.setAllItemsInSectio2(singleItem);
            }else if (i==2){
                dm.setHeaderTitle("All Videos");
                ArrayList<AllVideo> singleItem = new ArrayList<AllVideo>();
                for (int j = 0; j < allVideoArrayList.size(); j++) {
                    singleItem.add(new AllVideo("Item " + j));
                }

                dm.setAllItemsInSection1(singleItem);
            }else if (i==3){
                dm.setHeaderTitle("Latest Videos");
                ArrayList<LastestVideo> singleItem = new ArrayList<LastestVideo>();
                for (int j = 0; j < lastestVideoArrayList.size(); j++) {
                    singleItem.add(new LastestVideo("Item " + j));
                }

                dm.setAllItemsInSectio3(singleItem);
            }
            allSampleData.add(dm);
        }
    }

    public void createDataForTypeTwo() {

            SectionDataModel dm = new SectionDataModel();

                dm.setHeaderTitle("All Videos");
                ArrayList<AllVideo> singleItem = new ArrayList<AllVideo>();
                for (int j = 0; j < allVideoArrayList.size(); j++) {
                    singleItem.add(new AllVideo("Item " + j));
                }
                dm.setAllItemsInSection1(singleItem);

            allSampleData.add(dm);
    }



    @Override
    public void onStop() {
        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed
        mDemoSlider.stopAutoCycle();
        super.onStop();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public void getVideoList() {

        showProgreass();
        String url = String.format(ServerConfigStage.GET_VIDEO_LIST());
        RetrofitTask task = new RetrofitTask<VideoList>(HomeFragment.this, CommonUtility.HTTP_REQUEST_TYPE.GET, CommonUtility.CallerFunction.GET_VIDEO_LIST, url, getActivity());
        task.execute();

    }

    public void showProgreass() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.cancel();
    }

    @Override
    public void onRetrofitTaskComplete(Response<VideoList> response, Context context, CommonUtility.CallerFunction _callerFunction) {

        if (response.isSuccess()) {

            if (response.body() != null) {
                if(response.body().getStatusMessage().equals("Success")) {
                    VideoList videoList;
                    videoList = response.body();

                    allVideoArrayList = videoList.getAllVideos();
                    lastestVideoArrayList = videoList.getLastestVideos();
                    categoryVideoArrayList = videoList.getCategoryVideos();
                    if (userType.equals("Type 1")){
                    createDataForTypeOne();

                        my_recycler_view.setHasFixedSize(true);

                        RecyclerViewDataAdapter adapter = new RecyclerViewDataAdapter(getActivity(), allSampleData);

                        my_recycler_view.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

                        my_recycler_view.setAdapter(adapter);
                    }else if (userType.equals("Type 2")){
                        createDataForTypeTwo();

                        my_recycler_view.setHasFixedSize(true);

                        RecyclerViewDataAdapter adapter = new RecyclerViewDataAdapter(getActivity(), allSampleData);

                        my_recycler_view.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

                        my_recycler_view.setAdapter(adapter);
                    }
                    stopProgress();

                }
                    else {

                      Toast.makeText(getActivity(),response.body().getStatusMessage(),Toast.LENGTH_LONG).show();
                    }
                }


            }
        }


    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();

    }
}
