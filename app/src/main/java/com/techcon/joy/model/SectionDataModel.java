package com.techcon.joy.model;

import java.util.ArrayList;

/**
 * Created by pratap.kesaboyina on 30-11-2015.
 */
public class SectionDataModel {



    private String headerTitle;
    private ArrayList<VideoList> allItemsInSection;
    private ArrayList<AllVideo> allItemsInSection1;
    private ArrayList<CategoryVideo> allItemsInSection2;
    private ArrayList<LastestVideo> allItemsInSection3;

    public ArrayList<AllVideo> getAllItemsInSection1() {
        return allItemsInSection1;
    }

    public ArrayList<CategoryVideo> getAllItemsInSection2() {
        return allItemsInSection2;
    }

    public void setAllItemsInSection2(ArrayList<CategoryVideo> allItemsInSection2) {
        this.allItemsInSection2 = allItemsInSection2;
    }

    public ArrayList<LastestVideo> getAllItemsInSection3() {
        return allItemsInSection3;
    }

    public void setAllItemsInSection3(ArrayList<LastestVideo> allItemsInSection3) {
        this.allItemsInSection3 = allItemsInSection3;
    }

    public SectionDataModel() {

    }
    public SectionDataModel(String headerTitle, ArrayList<VideoList> allItemsInSection) {
        this.headerTitle = headerTitle;
        this.allItemsInSection = allItemsInSection;
    }



    public String getHeaderTitle() {
        return headerTitle;
    }

    public void setHeaderTitle(String headerTitle) {
        this.headerTitle = headerTitle;
    }

    public ArrayList<VideoList> getAllItemsInSection() {
        return allItemsInSection;
    }

    public void setAllItemsInSection(ArrayList<VideoList> allItemsInSection) {
        this.allItemsInSection = allItemsInSection;
    }
    public void setAllItemsInSection1(ArrayList<AllVideo> allItemsInSection1) {
        this.allItemsInSection1 = allItemsInSection1;
    }
    public void setAllItemsInSectio2(ArrayList<CategoryVideo> allItemsInSection2) {
        this.allItemsInSection2 = allItemsInSection2;
    }
    public void setAllItemsInSectio3(ArrayList<LastestVideo> allItemsInSection3) {
        this.allItemsInSection3 = allItemsInSection3;
    }


}