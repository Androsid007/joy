package com.techcon.joy.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LastestVideo {

    public LastestVideo(String name) {
        this.name = name;

    }

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("video_url")
    @Expose
    private String videoUrl;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

}
