package com.techcon.joy.model;

/**
 * Created by hp on 4/5/2017.
 */

public class VideoThumbnail {

    private String name;
    private String url;
    private String description;


    public VideoThumbnail() {
    }

    public VideoThumbnail(String name, String url) {
        this.name = name;
        this.url = url;
    }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}