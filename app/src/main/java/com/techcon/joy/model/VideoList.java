package com.techcon.joy.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VideoList {

    @SerializedName("category_videos")
    @Expose
    private List<CategoryVideo> categoryVideos = null;
    @SerializedName("lastest_videos")
    @Expose
    private List<LastestVideo> lastestVideos = null;
    @SerializedName("all_videos")
    @Expose
    private List<AllVideo> allVideos = null;
    @SerializedName("status_code")
    @Expose
    private Integer statusCode;
    @SerializedName("status_message")
    @Expose
    private String statusMessage;

    public List<CategoryVideo> getCategoryVideos() {
        return categoryVideos;
    }

    public void setCategoryVideos(List<CategoryVideo> categoryVideos) {
        this.categoryVideos = categoryVideos;
    }

    public List<LastestVideo> getLastestVideos() {
        return lastestVideos;
    }

    public void setLastestVideos(List<LastestVideo> lastestVideos) {
        this.lastestVideos = lastestVideos;
    }

    public List<AllVideo> getAllVideos() {
        return allVideos;
    }

    public void setAllVideos(List<AllVideo> allVideos) {
        this.allVideos = allVideos;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }
}
