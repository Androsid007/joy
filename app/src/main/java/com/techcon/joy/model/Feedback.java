package com.techcon.joy.model;

/**
 * Created by hp on 4/19/2017.
 */

public class Feedback {

    public String isConvenced;
    public String response;
    public String userID;
    public String caseID;

    public String getIsConvenced() {
        return isConvenced;
    }

    public void setIsConvenced(String isConvenced) {
        this.isConvenced = isConvenced;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getCaseID() {
        return caseID;
    }

    public void setCaseID(String caseID) {
        this.caseID = caseID;
    }
}
