package com.techcon.joy.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LikeParams {

    @SerializedName("videoid")
    @Expose
    private String videoid;
    @SerializedName("video_action")
    @Expose
    private String videoAction;
    @SerializedName("action_val")
    @Expose
    private String actionVal;
    @SerializedName("userid")
    @Expose
    private String userid;

    public String getVideoid() {
        return videoid;
    }

    public void setVideoid(String videoid) {
        this.videoid = videoid;
    }

    public String getVideoAction() {
        return videoAction;
    }

    public void setVideoAction(String videoAction) {
        this.videoAction = videoAction;
    }

    public String getActionVal() {
        return actionVal;
    }

    public void setActionVal(String actionVal) {
        this.actionVal = actionVal;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

}
