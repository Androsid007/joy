package com.techcon.joy.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.techcon.joy.R;
import com.techcon.joy.fragment.PlayerFragment;

public class PlayerActivity extends AppCompatActivity {

    private Toolbar toolbar;
    public static final String mypreference = "mypref";
    private String userType;
    private SharedPreferences sharedpreferences;
    private TextView titleTxt;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (android.os.Build.VERSION.SDK_INT >= 21){
        Window window = getWindow();


        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);


        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.my_statusbar_color));}

        sharedpreferences = getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        userType = sharedpreferences.getString("userType","");

        setContentView(R.layout.activity_player);
        toolbar=(Toolbar)findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        supportInvalidateOptionsMenu();

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.home:
                        if (userType.equals("Type 2")){
                        Intent profileIntent = new Intent(PlayerActivity.this, HomeActivity.class);
                        startActivity(profileIntent);
                        finish();
                        return true;
                        }

                }
                return false;
            }
        });

                Fragment newFragment;
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

                newFragment = new PlayerFragment();
                transaction.add(R.id.container, newFragment);
                transaction.commit();
    }

    @Override
    public void onBackPressed() {
        // do nothing
        if (userType.equals("Type 2")){
            super.onBackPressed();
        }
    }
}
