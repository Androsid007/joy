package com.techcon.joy.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.techcon.joy.NetworkService.CommonUtility;
import com.techcon.joy.NetworkService.RetrofitTask;
import com.techcon.joy.NetworkService.RetrofitTaskListener;
import com.techcon.joy.NetworkService.ServerConfigStage;
import com.techcon.joy.R;
import com.techcon.joy.model.UserInfoResponse;

import retrofit.Response;

public class ProfileActivity extends AppCompatActivity implements RetrofitTaskListener<UserInfoResponse> {

    private Toolbar toolbar;
    private ProgressDialog progressDialog;
    private String fName,mName,lName,pGender,pStatus,pNationality,pEmiratesId,pEmail,pPhone,LawyerName,LawyerId,LawyerPhone;
    private TextView name,gender,status,nationality,emiratesId,email,phone,lawyerName,lawyerId,lawyerPhone;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT >= 21){
        Window window = getWindow();


        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.my_statusbar_color));
        }
        setContentView(R.layout.activity_profile);

        toolbar=(Toolbar)findViewById(R.id.tool_bar);
        //session = new SessionManager(getApplicationContext());
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(R.mipmap.notification);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setTitle("Profile");

        name = (TextView)findViewById(R.id.nameTxt);
        gender = (TextView)findViewById(R.id.genderTxt);
        status = (TextView)findViewById(R.id.statusTxt);
        nationality = (TextView)findViewById(R.id.nationalityTxt);
        emiratesId = (TextView)findViewById(R.id.emiratesIdTxt);
        email = (TextView)findViewById(R.id.emailTxt);
        phone = (TextView)findViewById(R.id.numberTxt);
        lawyerName = (TextView)findViewById(R.id.lawyerNameTxt);
        lawyerId = (TextView)findViewById(R.id.lawyerIdTxt);
        lawyerPhone = (TextView)findViewById(R.id.lawyerPhoneTxt);

        getUserInfo();

    }

    public void getUserInfo() {

        showProgreass();
        String url = String.format(ServerConfigStage.GET_USER_INFO());
        RetrofitTask task = new RetrofitTask<UserInfoResponse>(ProfileActivity.this, CommonUtility.HTTP_REQUEST_TYPE.GET, CommonUtility.CallerFunction.GET_USER_INFO, url, ProfileActivity.this);
        task.execute();

    }

    public void showProgreass() {
        progressDialog = new ProgressDialog(ProfileActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.cancel();
    }


    @Override
    public void onRetrofitTaskComplete(Response<UserInfoResponse> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response.isSuccess()) {

            if (response.body() != null) {
                if(response.body().getStatusMessage().equals("Success")) {

                    fName = response.body().getUserdata().getPartyInfo().getFname();
                    lName = response.body().getUserdata().getPartyInfo().getLname();
                    mName = response.body().getUserdata().getPartyInfo().getMname();
                    pGender = response.body().getUserdata().getPartyInfo().getGender();
                    pStatus = response.body().getUserdata().getPartyInfo().getMaritalStatus();
                    pNationality = response.body().getUserdata().getPartyInfo().getNationality();
                    pEmiratesId = response.body().getUserdata().getPartyInfo().getEmiratedId();
                    pEmail = response.body().getUserdata().getPartyInfo().getEmailid();
                    pPhone = response.body().getUserdata().getPartyInfo().getPhone();
                    LawyerName = response.body().getUserdata().getPartyInfo().getLawyerName();
                    LawyerId = response.body().getUserdata().getPartyInfo().getEmailid();
                    LawyerPhone = response.body().getUserdata().getPartyInfo().getLawyerPhone();

                    name.setText(fName+" "+mName+" "+lName);
                    gender.setText(pGender);
                    status.setText(pGender);
                    nationality.setText(pNationality);
                    emiratesId.setText(pEmiratesId);
                    email.setText(pEmail);
                    phone.setText(pPhone);
                    lawyerName.setText(LawyerName);
                    lawyerId.setText(LawyerId);
                    lawyerPhone.setText(LawyerPhone);



                }
                else {

                    Toast.makeText(this,response.body().getStatusMessage(),Toast.LENGTH_LONG).show();
                }
            }


        }
    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {

    }
}
