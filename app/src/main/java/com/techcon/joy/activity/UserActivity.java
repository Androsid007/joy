package com.techcon.joy.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.techcon.joy.R;

public class UserActivity extends AppCompatActivity {

    private Button newUser,existingUser;
    public static final String mypreference = "mypref";
    public static final String Name = "userType";
    private SharedPreferences sharedpreferences;
    private Boolean exit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.my_statusbar_color));
        }
        setContentView(R.layout.activity_user);

        sharedpreferences = getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);

        sharedpreferences.getString(Name, "");
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(Name, "User Type");
        editor.commit();

        newUser = (Button)findViewById(R.id.btnNewUser);
        existingUser = (Button)findViewById(R.id.btnExistingUser);


        newUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserActivity.this,SignUpActivity.class);
                startActivity(intent);
            }
        });

        existingUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserActivity.this,LoginActivity.class);
                startActivity(intent);

            }
        });
    }

    @Override
    public void onBackPressed() {
     if (exit) {
        finish(); // finish activity
    } else {
        Toast.makeText(this, "Press Back again to Exit.",
                Toast.LENGTH_SHORT).show();
        exit = true;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                exit = false;
            }
        }, 3 * 1000);

    }
    }
}
