package com.techcon.joy.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.techcon.joy.NetworkService.CommonUtility;
import com.techcon.joy.NetworkService.RetrofitTask;
import com.techcon.joy.NetworkService.RetrofitTaskListener;
import com.techcon.joy.NetworkService.ServerConfigStage;
import com.techcon.joy.NetworkService.SessionManager;
import com.techcon.joy.R;
import com.techcon.joy.model.Register;
import com.techcon.joy.model.RegisterParams;

import retrofit.Response;

import static com.techcon.joy.activity.UserActivity.Name;

public class SignUpActivity extends AppCompatActivity implements RetrofitTaskListener<Register> {

    private Button register;
    public static final String mypreference = "mypref";
    private String userType;
    private SharedPreferences sharedpreferences;
    private ProgressDialog progressDialog;
    private String userName,firstName,lastName,emailid,password,gender,phone;
    private EditText uName,fName,lName,emailId,pass;
    private SessionManager session;
    private RadioButton male,female;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    RadioGroup radio;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();


            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);


            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.my_statusbar_color));
        }
        setContentView(R.layout.activity_sign_up);
        session = new SessionManager(this);

        uName = (EditText)findViewById(R.id.userName);
        //fName = (EditText)findViewById(R.id.firstName);
        lName = (EditText)findViewById(R.id.lastName);
        emailId = (EditText)findViewById(R.id.email);
        pass = (EditText)findViewById(R.id.password);
        male = (RadioButton)findViewById(R.id.male);
        female = (RadioButton)findViewById(R.id.female);
        radio = (RadioGroup) findViewById(R.id.radio);


      /*  radio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.male:
                        // do operations specific to this selection
                        break;
                    case R.id.female:
                        // do operations specific to this selection
                        break;

                }
            }
        });
*/

        register = (Button)findViewById(R.id.btnRegister);
        sharedpreferences = getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        userType = sharedpreferences.getString("userType","");

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userName = uName.getText().toString().trim();
                password = pass.getText().toString().trim();
                emailid=  emailId.getText().toString().trim();
                phone=lName.getText().toString().trim();
                // get selected radio button from radioGroup
                int selectedId = radio.getCheckedRadioButtonId();

                // find the radio button by returned id
                RadioButton radioButton = (RadioButton) findViewById(selectedId);


                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString(Name, "Type 2");
                editor.commit();
                String url = String.format(ServerConfigStage.GET_REGISTER());
                if (validate()) {

                    callRegisterService(url);
                }

            }
        });
    }
    public void showProgreass() {
        progressDialog = new ProgressDialog(SignUpActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.cancel();
    }
    public boolean validate() {
        boolean valid = true;




        if (userName.isEmpty() ) {
            uName.setError("Please enter your name");
            uName.requestFocus();
            valid = false;
        } else if (password.isEmpty() || password.length() < 7) {
            pass.setError("Enter at least 8 characters");
            pass.requestFocus();

            valid = false;
        } else  if (emailid.isEmpty() || !emailid.matches(emailPattern)) {
            emailId.setError("Enter a valid email address");
            emailId.requestFocus();
            valid = false;
        }
        else if (phone.isEmpty() ) {
            lName.setError("Enter valid phone number");
            lName.requestFocus();
            valid = false;
        }
        return valid;
    }

    public void callRegisterService(String url) {
        showProgreass();

        RegisterParams registerParams =new RegisterParams();
        registerParams.setUsername(userName);
        registerParams.setFirstName("d");
        registerParams.setLastName("kk");
        registerParams.setEmailid(emailid);
        registerParams.setPassword(password);
        registerParams.setGender("male");
        registerParams.setPhoto("");


        try {
            url = url.replace(" ", "%20");
            RetrofitTask task = new RetrofitTask<Register>(SignUpActivity.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.REGISTER, url, this, registerParams);
            task.execute();



        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRetrofitTaskComplete(Response<Register> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response.isSuccess()) {
            if (response.body() != null) {

                if (response.body().getStatusMessage().equals("Success")){
                    session.setLogin(true);
                    //if (response.body().getStatusMessage().equals("Success"))
                    Toast.makeText(SignUpActivity.this,"Successfully Registered",Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(SignUpActivity.this,HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    intent.putExtra("type",userType);
                    startActivity(intent);
                }else {
                    Toast.makeText(SignUpActivity.this,response.body().getStatusMessage(),Toast.LENGTH_LONG).show();
                }
            }
        }

    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
    stopProgress();
    }
}
