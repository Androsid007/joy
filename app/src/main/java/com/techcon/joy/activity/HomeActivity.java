package com.techcon.joy.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.techcon.joy.NetworkService.SessionManager;
import com.techcon.joy.R;
import com.techcon.joy.fragment.FeedbackFragment;
import com.techcon.joy.fragment.HomeFragment;
import com.techcon.joy.fragment.LogoutFragment;
import com.techcon.joy.fragment.PrivacyPolicyFragment;
import com.techcon.joy.fragment.SettingsFragment;
import com.techcon.joy.fragment.TermsConditionFragment;

public class HomeActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    public static final String mypreference = "mypref";
    private String userType;
    private SharedPreferences sharedpreferences;
    private NavigationView navigationView;
    private SessionManager session;
    //private Fragment newFragment;
    //private FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();

            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.my_statusbar_color));
        }
        setContentView(R.layout.activity_home);
        session = new SessionManager(this);

        sharedpreferences = getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        userType = sharedpreferences.getString("userType","");


        toolbar=(Toolbar)findViewById(R.id.tool_bar);
        //session = new SessionManager(getApplicationContext());
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(R.mipmap.notification);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setTitle("Videos");
        initNavigationDrawer();

        Fragment newFragment;
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        newFragment = new HomeFragment();
        transaction.add(R.id.flContent, newFragment);
        transaction.commit();

    }

    private void hideItem()
    {
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.feedback).setVisible(false);
    }

    public void initNavigationDrawer() {

         navigationView = (NavigationView)findViewById(R.id.navigation_view);
        navigationView.setItemIconTintList(null);
        if (userType.equals("Type 2")){
            hideItem();
        }
        View headerview = navigationView.getHeaderView(0);
        ImageView profile = (ImageView)headerview.findViewById(R.id.profile);
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              Intent intent = new Intent(HomeActivity.this,ProfileActivity.class);
                startActivity(intent);
                drawerLayout.closeDrawers();
            }
        });
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                Fragment newFragment;
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

                int id = menuItem.getItemId();

                switch (id){
                    case R.id.home:
                        newFragment = new HomeFragment();
                        transaction.add(R.id.flContent, newFragment);
                        transaction.commit();
                        Toast.makeText(getApplicationContext(),"Home Clicked", Toast.LENGTH_SHORT).show();
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.feedback:
                        newFragment = new FeedbackFragment();
                        transaction.add(R.id.flContent, newFragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                        Toast.makeText(getApplicationContext(),"Feedback Clicked", Toast.LENGTH_SHORT).show();
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.tnc:
                        newFragment = new TermsConditionFragment();
                        transaction.add(R.id.flContent, newFragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                        Toast.makeText(getApplicationContext(),"Terms & Condition Clicked",Toast.LENGTH_SHORT).show();
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.pnp:
                        newFragment = new PrivacyPolicyFragment();
                        transaction.add(R.id.flContent, newFragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                        Toast.makeText(getApplicationContext(),"Privacy Policy Clicked",Toast.LENGTH_SHORT).show();
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.setting:
                        newFragment = new SettingsFragment();
                        transaction.add(R.id.flContent, newFragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                        Toast.makeText(getApplicationContext(),"Setting Clicked",Toast.LENGTH_SHORT).show();
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.logout:
                        newFragment = new LogoutFragment();
                        transaction.add(R.id.flContent, newFragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                        drawerLayout.closeDrawers();
                        Toast.makeText(getApplicationContext(),"Logged Out",Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(HomeActivity.this,LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent);

                }
                return true;
            }
        });
        View header = navigationView.getHeaderView(0);
        TextView tv_email = (TextView)header.findViewById(R.id.tv_email);
        tv_email.setText("User Nadeem");
        drawerLayout = (DrawerLayout)findViewById(R.id.drawer);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.drawer:
                drawerLayout.openDrawer(GravityCompat.END);  // OPEN DRAWER
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.drawer).setEnabled(true);


        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
       if (session.isLoggedIn()){
           finish();
       }
    }

}
