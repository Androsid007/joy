package com.techcon.joy.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.techcon.joy.NetworkService.CommonUtility;
import com.techcon.joy.NetworkService.RetrofitTask;
import com.techcon.joy.NetworkService.RetrofitTaskListener;
import com.techcon.joy.NetworkService.ServerConfigStage;
import com.techcon.joy.NetworkService.SessionManager;
import com.techcon.joy.R;
import com.techcon.joy.model.Login;
import com.techcon.joy.model.UserInfoResponse;

import retrofit.Response;

import static com.techcon.joy.activity.UserActivity.Name;

public class LoginActivity extends AppCompatActivity implements RetrofitTaskListener<UserInfoResponse>{
    private EditText emailTxt,passwordTxt;
    private Button login;
    private Toolbar toolbar;
    private String emailid,password;
    private Boolean exit = false;
    public static final String mypreference = "mypref";
    private String userType,userId,caseId;
    private SharedPreferences sharedpreferences;
    private ProgressDialog progressDialog;
    private SessionManager session;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.my_statusbar_color));
        }
        setContentView(R.layout.activity_login);
        toolbar=(Toolbar)findViewById(R.id.tool_bar);
        session = new SessionManager(this);

        emailTxt = (EditText)findViewById(R.id.emailEditTxt);

        passwordTxt = (EditText)findViewById(R.id.passwordEditTxt);
        login = (Button)findViewById(R.id.btnLogin);

        sharedpreferences = getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        userType = sharedpreferences.getString("userType","");
        userId = sharedpreferences.getString("userId","");
        caseId = sharedpreferences.getString("caseId","");


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emailid = emailTxt.getText().toString().trim();
                password = passwordTxt.getText().toString().trim();
                if (emailid.isEmpty()||password.isEmpty()){
                    Toast.makeText(LoginActivity.this,"Please enter all fields",Toast.LENGTH_LONG).show();
                }else if (isValidEmail(emailid)){
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString(Name, "Type 1");
                    editor.commit();

                    String url = String.format(ServerConfigStage.GET_LOGIN(),emailid,password);
                    callLoginService(url);
                }else {
                    Toast.makeText(LoginActivity.this,"Please enter valid email",Toast.LENGTH_LONG).show();

                }




            }
        });
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public void showProgreass() {
        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.cancel();
    }

    public void callLoginService(String url) {
        showProgreass();

        Login login =new Login();
        login.setEmailid(emailid);
        login.setPassword(password);

        try {
            url = url.replace(" ", "%20");
            RetrofitTask task = new RetrofitTask<UserInfoResponse>(LoginActivity.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.LOGIN, url, this, login);
            task.execute();



        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:


            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public void onBackPressed() {
       super.onBackPressed();
    }


    @Override
    public void onRetrofitTaskComplete(Response<UserInfoResponse> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response.isSuccess()) {
            if (response.body() != null) {

                if (response.body().getStatusMessage().equals("Success")){
                    session.setLogin(true);
                //if (response.body().getStatusMessage().equals("Success"))
                Toast.makeText(LoginActivity.this,"Login Successfull",Toast.LENGTH_LONG).show();
                SharedPreferences.Editor editor = sharedpreferences.edit();
                    userId = response.body().getUserdata().getPartyInfo().getUserId();
                    caseId = response.body().getUserdata().getPartyInfo().getCaseId();
                editor.putString("userId",userId );
                    editor.putString("caseId",caseId);
                editor.commit();
                Intent intent = new Intent(LoginActivity.this,HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.putExtra("type",userType);
                startActivity(intent);
                }else {
                    Toast.makeText(LoginActivity.this,response.body().getStatusMessage(),Toast.LENGTH_LONG).show();
                }
            }
        }

    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();
    }
}
