package com.techcon.joy.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.techcon.joy.R;
import com.techcon.joy.activity.PlayerActivity;
import com.techcon.joy.model.VideoList;

import java.util.ArrayList;

public class CategoryVideoDataAdapter extends RecyclerView.Adapter<CategoryVideoDataAdapter.SingleItemRowHolder> {

    private ArrayList<VideoList> itemsList;
    private Context mContext;

    public CategoryVideoDataAdapter(Context context, ArrayList<VideoList> itemsList) {
        this.itemsList = itemsList;
        this.mContext = context;
    }

    @Override
    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_single_card, null);
        SingleItemRowHolder mh = new SingleItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(SingleItemRowHolder holder, int i) {

        //VideoList singleItem = itemsList.get(i);

        //holder.tvTitle.setText(singleItem.getCategoryVideos().get(i).getName());


       /* Glide.with(mContext)
                .load(feedItem.getImageURL())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .error(R.drawable.bg)
                .into(feedListRowHolder.thumbView);*/
    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView tvTitle;

        protected ImageView itemImage;


        public SingleItemRowHolder(View view) {
            super(view);


            this.itemImage = (ImageView) view.findViewById(R.id.itemImage);


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(mContext, PlayerActivity.class);
                    mContext.startActivity(intent);

                }
            });


        }

    }

}
