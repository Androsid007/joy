package com.techcon.joy.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.techcon.joy.R;
import com.techcon.joy.model.LastestVideo;
import com.techcon.joy.model.SectionDataModel;

import java.util.ArrayList;

public class RecyclerViewDataAdapter extends RecyclerView.Adapter<RecyclerViewDataAdapter.ItemRowHolder> {

    private ArrayList<SectionDataModel> dataList;
    private Context mContext;

    public RecyclerViewDataAdapter(Context context, ArrayList<SectionDataModel> dataList) {
        this.dataList = dataList;
        this.mContext = context;
    }

    @Override
    public ItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, null);
        ItemRowHolder mh = new ItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(ItemRowHolder itemRowHolder, int i) {

        final String sectionName = dataList.get(i).getHeaderTitle();

        switch (sectionName){
            case "Categorized Videos" :
                ArrayList singleSectionItems1 = dataList.get(i).getAllItemsInSection2();
                CategoryVideoDataAdapter itemListDataAdapter1 = new CategoryVideoDataAdapter(mContext, singleSectionItems1);

                itemRowHolder.recycler_view_list.setHasFixedSize(true);
                itemRowHolder.recycler_view_list.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
                itemRowHolder.recycler_view_list.setAdapter(itemListDataAdapter1);
                break;
            case "All Videos" :
                ArrayList singleSectionItems = dataList.get(i).getAllItemsInSection1();
                AllVideoDataAdapter itemListDataAdapter = new AllVideoDataAdapter(mContext, singleSectionItems);
                itemRowHolder.recycler_view_list.setHasFixedSize(true);
                itemRowHolder.recycler_view_list.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
                itemRowHolder.recycler_view_list.setAdapter(itemListDataAdapter);
                break;
            case "Latest Videos" :
                ArrayList singleSectionItems2 = dataList.get(i).getAllItemsInSection3();
                LatestVideoDataAdapter itemListDataAdapter2 = new LatestVideoDataAdapter(mContext, singleSectionItems2);
                itemRowHolder.recycler_view_list.setHasFixedSize(true);
                itemRowHolder.recycler_view_list.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
                itemRowHolder.recycler_view_list.setAdapter(itemListDataAdapter2);
                break;
        }

        itemRowHolder.itemTitle.setText(sectionName);

       /* Glide.with(mContext)
                .load(feedItem.getImageURL())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .error(R.drawable.bg)
                .into(feedListRowHolder.thumbView);*/
    }

    @Override
    public int getItemCount() {
        return (null != dataList ? dataList.size() : 0);
    }

    public class ItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView itemTitle;

        protected RecyclerView recycler_view_list;




        public ItemRowHolder(View view) {
            super(view);

            this.itemTitle = (TextView) view.findViewById(R.id.itemTitle);
            this.recycler_view_list = (RecyclerView) view.findViewById(R.id.recycler_view_list);
            //this.btnMore= (Button) view.findViewById(R.id.btnMore);


        }

    }

}
