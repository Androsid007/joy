package com.techcon.joy.NetworkService;

import android.content.Context;


import retrofit.Response;

public interface RetrofitTaskListener<M> {
    public void onRetrofitTaskComplete(Response<M> response, Context context, CommonUtility.CallerFunction _callerFunction);
    public void onRetrofitTaskFailure(Throwable t);
}
