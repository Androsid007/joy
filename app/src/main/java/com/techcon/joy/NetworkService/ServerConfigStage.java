package com.techcon.joy.NetworkService;

public class ServerConfigStage {

    public static String GET_VIDEO_LIST() {
        return "http://vl.techconlabs.com/joy/rest_api/videos/1";
    }

    public static String GET_USER_INFO(){
        return "http://vl.techconlabs.com/joy/rest_api/user/1";
    }

    public static String GET_LOGIN(){
        return "http://vl.techconlabs.com/joy/rest_api/login";
    }

    public static String SEND_FEEDBACK(){
        return "http://vl.techconlabs.com/joy/rest_api/feedback";
    }

    public static String GET_FAST_ORDER(){
        return "http://64.71.180.21/FastOrderCust.ashx/?%s/%s/%s";
    }

    public static String GET_REGISTER(){
        return "http://vl.techconlabs.com/joy/rest_api/register";
    }

    public static String GET_LIKE(){
        return "http://vl.techconlabs.com/joy/rest_api/user_review";
    }

}