package com.techcon.joy.NetworkService;



import com.techcon.joy.model.Feedback;
import com.techcon.joy.model.FeedbackResponse;
import com.techcon.joy.model.Like;
import com.techcon.joy.model.LikeParams;
import com.techcon.joy.model.Login;
import com.techcon.joy.model.Register;
import com.techcon.joy.model.RegisterParams;
import com.techcon.joy.model.UserInfoResponse;
import com.techcon.joy.model.VideoList;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Url;


public interface RetrofitApiInterface<M> {
    @GET
    Call<VideoList> getVideoList(@Url String url);
    @GET
    Call<UserInfoResponse> getUserInfo(@Url String url);
    @POST
    Call<UserInfoResponse>  getLogin(@Body Login login, @Url String url);
    @POST
    Call<FeedbackResponse>  sendFeedback(@Body Feedback feedback, @Url String url);
    @POST
    Call<Register>  getRegister(@Body RegisterParams register, @Url String url);
    @POST
    Call<Like>  getLike(@Body LikeParams likeParams, @Url String url);
    @POST
    Call<Like>  getRate(@Body LikeParams likeParams, @Url String url);

}
