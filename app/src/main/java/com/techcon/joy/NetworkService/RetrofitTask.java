package com.techcon.joy.NetworkService;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.techcon.joy.model.Feedback;
import com.techcon.joy.model.LikeParams;
import com.techcon.joy.model.Login;
import com.techcon.joy.model.Register;
import com.techcon.joy.model.RegisterParams;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;


public class RetrofitTask<M> implements Callback<M> {


    private RetrofitTaskListener<M> _callback;
    private Context _context;

    private CommonUtility.HTTP_REQUEST_TYPE _methodType;
    private CommonUtility.CallerFunction _callerFunction;
    private  Object Fast;

    M genricList;

    private String URL;
    private Object postParamsEntity;
    long preExecutionTime, totalExecutionTime;
    boolean shouldAllowRetry = true;
    int timeOut;

    public RetrofitTask(RetrofitTaskListener<M> callback,
                        CommonUtility.HTTP_REQUEST_TYPE methodType, CommonUtility.CallerFunction callerFunction,
                        String url, Context context) {
        this._callback = callback;
        this._methodType = methodType;
        this.URL = url;
        _context = context;
        this._callerFunction = callerFunction;
        timeOut = CommonUtility.RETROFIT_TIMEOUT;
    }

    public RetrofitTask(RetrofitTaskListener<M> callback,
                        CommonUtility.HTTP_REQUEST_TYPE methodType, CommonUtility.CallerFunction callerFunction,
                        String url, Context context, Object postParamsEntity) {
        this._callback = callback;
        this._methodType = methodType;
        this.URL = url;
        timeOut = CommonUtility.RETROFIT_TIMEOUT;
        _context = context;
        this._callerFunction = callerFunction;
        this.postParamsEntity = postParamsEntity;
    }

    public void execute() {
        executeTask(timeOut);
    }


    private void executeTask(int timeOut) {

        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setConnectTimeout(timeOut, TimeUnit.SECONDS);
        okHttpClient.setReadTimeout(timeOut, TimeUnit.SECONDS);
        okHttpClient.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder().url(original.url());
                //.addHeader("User-Agent", "couponlooto_android_app");
                preExecutionTime = System.currentTimeMillis();
                Request request = requestBuilder.build();
                Response response = chain.proceed(request);

                long totalExecutionTime = System.currentTimeMillis() - preExecutionTime;
                Log.i("okhttp", "preExecutionTime " + preExecutionTime);
                Log.i("okhttp", "postExecutionTime" + "" + System.currentTimeMillis() + "\ntotalExecutionTime " + totalExecutionTime + " ms");
                //GoogleAnalyticsConfig.createUserTimingEvent(_context, _callerFunction.name(), totalExecutionTime);
                return response;
            }
        });
        //Gson gson = new GsonBuilder().setLenient().create();

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();


        final Retrofit client = new Retrofit.Builder()
                .baseUrl(URLConfig.getBaseUrl())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build();
        RetrofitApiInterface service = client.create(RetrofitApiInterface.class);

        Call<M> call = null;
        switch (_callerFunction) {
            case GET_VIDEO_LIST:
                call = service.getVideoList(URL);
                break;
            case GET_USER_INFO:
                call = service.getUserInfo(URL);
                break;
            case LOGIN:
                call = service.getLogin( (Login) postParamsEntity, URL);
                break;
            case FEEDBACK:
                call = service.sendFeedback((Feedback) postParamsEntity,URL);
                break;
            case REGISTER:
                call = service.getRegister((RegisterParams) postParamsEntity,URL);
                break;
            case LIKE:
                call = service.getLike((LikeParams) postParamsEntity,URL);
                break;

        }
        call.enqueue(this);
    }

    @Override
    public void onResponse(retrofit.Response<M> response, Retrofit retrofit) {
        _callback.onRetrofitTaskComplete(response, _context, _callerFunction);
    }

    @Override
    public void onFailure(Throwable t) {

        _callback.onRetrofitTaskFailure(t);
    }
}